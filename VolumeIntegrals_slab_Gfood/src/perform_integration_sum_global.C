#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "util_Table.h"
#include <assert.h>
#include <math.h>
#include <stdlib.h>

int number_of_reductions(int which_integral);

void VIslab_DoSum(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int which_integral = NumIntegrals - *IntegralCounter + 1;

  /* FIXME: Add this symmetry stuff... Should be straightforward. */
  CCTK_REAL sym_factor1,sym_factor2,sym_factor3;

  if (CCTK_EQUALS(domain,"bitant")){
    sym_factor1 = 2.0e0;
    sym_factor2 = 2.0e0;
    sym_factor3 = 0.0e0;
  } else if (CCTK_EQUALS(domain,"octant")){
    sym_factor1 = 8.0e0;
    sym_factor2 = 0.0e0;
    sym_factor3 = 0.0e0;
  } else {
    sym_factor1 = 1.0e0;
    sym_factor2 = 1.0e0;
    sym_factor3 = 1.0e0;
  }

  CCTK_REAL d3x = cctk_delta_space[0]*cctk_delta_space[1]*cctk_delta_space[2];
  /* Note: Must edit number_of_reductions() when adding new integrands!
     This function is defined in number_of_reductions.C */
  int num_reductions=number_of_reductions(which_integral);

  if(verbose>=1) printf("VolumeIntegrals: Iter %d, num_reductions=%d, Integ. quantity=%s, Slab xmin/max @ (%e,%e); ymin/max @ (%e,%e); zmin/max @ (%e,%e).\n",
			which_integral,num_reductions,Integration_quantity_keyword[which_integral],
                        volintegral_slab_Gfood__x_min[which_integral],
                        volintegral_slab_Gfood__x_max[which_integral],
                        volintegral_slab_Gfood__y_min[which_integral],
                        volintegral_slab_Gfood__y_max[which_integral],
                        volintegral_slab_Gfood__z_min[which_integral],
                        volintegral_slab_Gfood__z_max[which_integral]);

  /* Perform the reduction sums across all MPI processes */
  int reduction_handle = CCTK_ReductionHandle("sum");

  for(int i=0;i<num_reductions;i++) {
    char integralname[100]; sprintf(integralname,"VolumeIntegrals_slab_Gfood::VolIntegrand%d",i+1);
    int varindex = CCTK_VarIndex(integralname);
    int ierr=0;
    assert(varindex>=0);
    ierr = CCTK_Reduce(cctkGH, -1, reduction_handle,
		       1, CCTK_VARIABLE_REAL, (void *)&VolIntegral[4*(which_integral) + i], 1, varindex);
    assert(!ierr);

    VolIntegral[4*(which_integral) + i]*=d3x; // <- Multiply the integrand by d3x

    if(verbose==2) printf("VolumeIntegrals: Iteration %d, reduction %d of %d. Reduction value=%e\n",which_integral,i,num_reductions,VolIntegral[4*(which_integral) + i]);

  }

  for(int i=0;i<num_reductions;i++) VolIntegral[4*(which_integral) + i] *= sym_factor1;
}
