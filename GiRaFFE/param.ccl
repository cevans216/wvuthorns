# Parameter definitions for thorn GiRaFFE

shares: ADMBase
USES CCTK_INT lapse_timelevels
USES CCTK_INT shift_timelevels
USES CCTK_INT metric_timelevels

#########################################################
# Timestepping (MoL) stuff
shares: MethodOfLines

USES CCTK_INT MoL_Num_Evolved_Vars

restricted:

CCTK_INT GiRaFFE_MaxNumEvolvedVars "The maximum number of evolved variables used by BSSN" ACCUMULATOR-BASE=MethodofLines::MoL_Num_Evolved_Vars
{
  7:7           :: "Just 7: mhd_st_x,mhd_st_y,mhd_st_z,Ax,Ay,Az, and psi6phi"
} 7
#########################################################

restricted:

#########################################################
# Set the drift velocity perpendicular to the current sheet to zero.
BOOLEAN current_sheet_null_v "Shall we null the velocity normal to the current sheet?"
{
} "no" #Necessary for the split monopole
#########################################################

#########################################################
# SPEED LIMIT: Set maximum relativistic gamma factor
# 
REAL GAMMA_SPEED_LIMIT "Maximum relativistic gamma factor. Note the default is much higher than IllinoisGRMHD. (GRFFE can handle higher Lorentz factors)"
{
 1:* :: "Positive > 1, though you'll likely have troubles far above 2000."
} 2000.0
#########################################################

#########################################################
# OUTER BOUNDARY CONDITION CHOICE
KEYWORD Velocity_BC "Chosen fluid velocity boundary condition"
{
  "outflow"        :: "Outflow boundary conditions"
  "copy"           :: "Copy data from nearest boundary point"
  "frozen"         :: "Frozen boundaries"
} "outflow"

KEYWORD EM_BC "EM field boundary condition"
{
  "copy"           :: "Copy data from nearest boundary point"
  "frozen"         :: "Frozen boundaries"
} "copy"
#########################################################


#########################################################
# SYMMETRY BOUNDARY PARAMS. Needed for handling staggered gridfunctions.
KEYWORD Symmetry "Symmetry: 0=NOSYMM, 1=EQUATORIAL. FIXME: Use ET symmetry interface. PROBLEM: ET does not support staggered gridfunctions"
{
  "none"       :: "no symmetry, full 3d domain"
  "equatorial" :: "NOT READY FOR PRIME TIME: reflection across z=0"
} "none"

REAL Sym_Bz "Symmetry parameter across z axis for magnetic fields = +/- 1"
{
 -1.0:1.0 :: "Set to +1 or -1."
} 1.0
#########################################################

REAL R_NS_aligned_rotator "The radius of the NS set by hand for the aligned rotator solution test." STEERABLE=ALWAYS
{
  *:* :: "any real"
} -1e100

REAL B_p_aligned_rotator "The magnitude of the poloidal magnetic field." STEERABLE=ALWAYS
{
  *:* :: "any real"
} 1e-5

REAL Omega_aligned_rotator "The angular velocity for the aligned rotator solution test." STEERABLE=ALWAYS
{
  *:* :: "any real"
} 1e3

###############################################################################################
private:

#########################################################
# EVOLUTION PARAMS
REAL damp_lorenz "Damping factor for Lorenz gauge. Has units of 1/length = 1/M. Try using the same value as used for the eta parameter, or smaller! Brian tried 0.1 with ADM mass of 1." STEERABLE=ALWAYS
{
 *:* :: "any real"
} 0.0
#########################################################
